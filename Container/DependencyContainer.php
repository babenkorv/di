<?php

namespace DC\Container;

class DependencyContainer
{
    private $definitions = [];
    private $container = [];

    public function get($name)
    {
        if (isset($this->container[$name]) && $this->container[$name]['shared']) {
            return $this->container[$name]['value'];
        }

        if (is_callable($this->definitions[$name]['value'])) {
            $this->container[$name] = [
                'value' => call_user_func($this->definitions[$name]['value'], $this),
                'shared' => $this->definitions[$name]['shared'],
            ];
            return $this->container[$name]['value'];
        }

        if (class_exists($this->definitions[$name]['value'])) {
            $reflection = new \ReflectionClass($this->definitions[$name]['value']);
            $constructor = $reflection->getConstructor();
            $args = [];
            foreach ($constructor->getParameters() as $parameter) {
                $parameterValue = null;
                $parameterClass = $parameter->getClass();
                $parameterValue = ($parameterClass) ? $this->get($parameterClass->getName()) : null;

                if (is_null($parameterValue) && $parameter->isDefaultValueAvailable()) {
                    $parameterValue = $parameter->getDefaultValue();
                }

                $args[] = $parameterValue;
            }

            $this->container[$name] = [
                'value' => $reflection->newInstanceArgs($args),
                'shared' => $this->definitions[$name]['shared'],
            ];
        }

        return $this->container[$name]['value'];
    }


    public function set($name, $value, $shared = true)
    {
        if (!empty($this->definitions[$name]['shared'])) {
            return false;
        }

        $this->definitions[$name] = [
            'value' => $value,
            'shared' => $shared
        ];
    }

}