<?php

use DC\Container\DependencyContainer;

require_once 'vendor/autoload.php';

$container = new DC\Container\DependencyContainer();

$container->set('A', '\DC\TestClasses\A', false);
$container->set('DC\TestClasses\InterfaceA
', '\DC\TestClasses\A', false);
$container->set('A', function (DependencyContainer $container) {
    return new \DC\TestClasses\A();
});
$a = $container->get('A');
$a->test();
$container->set('B', '\DC\TestClasses\B');

$b = $container->get('B');
$b->test();