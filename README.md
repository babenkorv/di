<h1> DI/DC </h1>

implementation of the DI/DC

<h3> How does it use? </h3>

Class can be stored using callback function. This function has to return an object of stored class. 
See example of callback bellow: 

<code>
$container->set('A', function (DependencyContainer $container) {
    return new A();
});
</code>


If in the "set" method input a name of the class. Container will try to find class via class name. Class name should be specified using psr4 standard.
See example of  bellow: 

<code>
$container->set('A', '\DC\TestClasses\A', false);
</code>

The second argument of the <bold> set </bold> method determines whether the method should exist in one instance

If the class argument is an object, it will be created automatically.

If the class argument is an interface, it should be defined using the "set" method.

in order to get the class object you need to use the "get" method.